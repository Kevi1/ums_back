from django.urls import include, path
from .views import *
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'post', PostViewSet,basename="posts")
router.register(r'subject', SubjectViewSet,basename="subjects")
router.register(r'faculty', FacultyViewSet,basename="facultys")
router.register(r'degree', DegreeViewSet,basename="degrees")

# Function views
#     1. Add an import:  from my_app import views
#     2. Add a URL to urlpatterns:  path('', views.home, name='home')



urlpatterns = [
    path('', include(router.urls)),
    path('get_user_data', view=get_user_data),

]
