from django.db import models

# Create your models here.

# python manage.py makemigrations
# python manage.py migrate

from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    ROLE_CHOICES = [
        ('student', 'Student'),
        ('admin', 'Administrator'),
        (3, 'Third'),
    ]
    role = models.CharField(max_length=130,choices=ROLE_CHOICES)

class Post(models.Model):
    title = models.CharField(max_length= 16)
    description = models.TextField()

class Subject(models.Model):
    title = models.CharField(max_length= 16)
    credits = models.IntegerField()
    code = models.CharField(max_length= 16)
    description = models.TextField()

class Faculty(models.Model):
    name = models.CharField(max_length= 16)
    # degree = 

class Degree(models.Model):
    YEAR_IN_SCHOOL_CHOICES = [
        (1, 'First'),
        (2, 'Second'),
        (3, 'Third'),
    ]
    DEGREE_TYPE_CHOICES= [
        ('bachelor', 'Bachelor'),
        ('master', 'Master'),
        # (3, 'Third'),
    ]
    name = models.CharField(max_length= 30)
    year = models.IntegerField(choices=YEAR_IN_SCHOOL_CHOICES)
    type = models.CharField(max_length = 20 ,choices=DEGREE_TYPE_CHOICES)
    faculty = models.ForeignKey(Faculty, on_delete=models.DO_NOTHING)
